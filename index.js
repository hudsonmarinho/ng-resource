var inflect = require('inflection-extended');

module.exports = function (app, config) {
    'use strict';

    var routes, resources, method, route, resource, path, file;

    resources          = {};
    routes             = [];

    config               = config || {};
    config.downloadUrl   = config.downloadUrl || '/resources.js';
    config.moduleName    = config.moduleName  || 'resources';
    config.paramDefaults = config.paramDefaults || {};
    config.actions       = config.actions || {};

    for (method in app.routes) {
        if (app.routes.hasOwnProperty(method)) {
            for (route = 0; route < app.routes[method].length; route = route + 1) {
                routes.push({method : method, path : app.routes[method][route].path, slices : app.routes[method][route].path.split('/')});
            }
        }
    }

    resources = routes.filter(function (route) {
        return route.slices.length > 1 && route.slices[route.slices.length - 1].substring(0,1) === ':';
    }).map(function (route) {
        return route.slices[route.slices.length - 2];
    }).reduce(function (resources, resource) {
        return resources.concat(resource);
    }, []).filter(function (resource, index, resources) {
        return resources.indexOf(resource) === index;
    }).map(function (resource, index, resources) {
        return {name : resource, routes : routes.filter(function (route) {
            return (route.slices.length > 0 && route.slices[route.slices.length - 1] === resource) ||
                   (route.slices.length > 1 && route.slices[route.slices.length - 2] === resource) ||
                   (route.slices.length > 2 && route.slices[route.slices.length - 3] === resource && resources.indexOf(route.slices[route.slices.length - 1]) === -1);
        })};
    });

    for (resource = 0; resource < resources.length; resource += 1) {
        config.paramDefaults[inflect.classify(resources[resource].name.replace(/\-/g, '_'))] = config.paramDefaults[inflect.classify(resources[resource].name.replace(/\-/g, '_'))] || {};
    }

    file = '(function () {\'use strict\';angular.module(\'' + config.moduleName + '\', [\'ngResource\'])';
    for (resource = 0; resource < resources.length; resource += 1) {
        for (route = 0; route < resources[resource].routes.length; route += 1) {
            if (resources[resource].routes[route].slices[resources[resource].routes[route].slices.length - 1].substring(0,1) === ':') {
                path = resources[resource].routes[route].path;
            }
        }
        file += '.factory(\'' + inflect.classify(resources[resource].name.replace(/\-/g, '_')) + '\', function ($resource) { return $resource(\'' + path + '\', ' + JSON.stringify(config.paramDefaults[inflect.classify(resources[resource].name)]) + ', ';
        for (route = 0; route < resources[resource].routes.length; route += 1) {
            if (resources[resource].routes[route].slices.length > 2 && resources[resource].routes[route].slices[resources[resource].routes[route].slices.length - 3] === resources[resource].name) {
                config.actions[inflect.classify(resources[resource].name)][inflect.classify(resources[resource].routes[route].slices[resources[resource].routes[route].slices.length - 1].replace(/\-/g, '_'))] = {
                    'method' : resources[resource].routes[route].method,
                    'url' : resources[resource].routes[route].path,
                    'isArray' : inflect.classify(resources[resource].routes[route].slices[resources[resource].routes[route].slices.length - 1].replace(/\-/g, '_')) !== resources[resource].routes[route].slices[resources[resource].routes[route].slices.length - 1].replace(/\-/g, '_')
                };
            }
        }
        file += JSON.stringify(config.actions[inflect.classify(resources[resource].name)]);
        file += ') })';
    }
    file += '}).call();';

    app.get(config.downloadUrl, function (request, response) {
        response.set('Content-Type', 'text/javascript');
        response.send(file);
    });
};
