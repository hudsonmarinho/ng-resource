ng-resource
================

Generates angularjs resources for all entities based on rest

NOTE: YOU DON'T HAVE TO INCLUDE THE FACEBOOK SDK SCRIPT LOADER

Usage example

Server.js
```js
var express = require('express');
var app = new express();

app.get('/', function () {});

app.post('/users', function () {});

app.get('/users', function () {});

app.get('/users/:userid', function () {});

app.put('/users/:userid', function () {});

app.del('/users/:userid', function () {});

app.post('/users/:userid/login', function () {});

app.post('/users/:userid/pictures', function () {});

app.post('/users/:userid/purchases', function () {});

app.get('/users/:userid/purchases', function () {});

app.get('/users/:userid/purchases/:purchaseid', function () {});

app.put('/users/:userid/purchases/:purchaseid', function () {});

app.del('/users/:userid/purchases/:purchaseid', function () {});

app.post('/users/:id/purchases/:purchaseid/items', function () {});

app.get('/users/:id/purchases/:purchaseid/items', function () {});

app.get('/users/:id/purchases/:purchaseid/items/:itemid', function () {});

app.put('/users/:id/purchases/:purchaseid/items/:itemid', function () {});

app.del('/users/:id/purchases/:purchaseid/items/:itemid', function () {});

app.post('/products', function () {});

app.get('/products', function () {});

app.get('/products/:productid', function () {});

app.put('/products/:productid', function () {});

app.del('/products/:productid', function () {});

ngResource(app, {
    downloadUrl : 'my-resources.js',
    moduleName : 'my-resources',
    paramDefaults : {
        user : {
            'userId' : '@_id'
        }
    }
});

app.listen(8000);
```

Controller.js
```js
angular.module('my-app', ['resources']).controller('my-controller', function (users, purchases, items, products) {
    user.query(function (users) {
        //...
    });

    purchases.query({userid : 'test'}, function (users) {
        //...
    });

    //...
});
```
